CREATE database IF NOT EXISTS `collegefest`;
USE `collegefest`;

CREATE TABLE `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `department` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `student` VALUES 
	(1,'Suresh','B.tech','India'),
	(2,'Muri','B.Arch','Canada'),
	(3,'Daniel','B.tech','New Zealand'),
	(4,'Tanya','B.Com','USA');

CREATE TABLE `roles` (
`role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
)ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
-- SQL query to populate the tables created by Spring 
INSERT INTO `collegefest`.`roles` (`role_id`,`name`) VALUES (1,'ADMIN');
INSERT INTO `collegefest`.`roles` (`role_id`,`name`) VALUES (2,'USER');

CREATE TABLE `users` (
`user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
)ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `collegefest`.`users` (`user_id`,`password`,`username`) VALUES (1, '$2a$12$W4troexnHC7LIOBSM1nmDuncp3tNmifMyJ58bKS.5zTYyo.g1svFO','admin');
INSERT INTO `collegefest`.`users` (`user_id`,`password`,`username`) VALUES (2, '$2a$12$9MutAqlt3FwEl2qdGyQPfOhCg18ggEA2rugzQxwmPV7Eb0dgMqbY.','user');


INSERT INTO `collegefest`.`users_roles` (`user_id`,`role_id`) VALUES (1, 1);
INSERT INTO `collegefest`.`users_roles` (`user_id`,`role_id`) VALUES (2, 2);