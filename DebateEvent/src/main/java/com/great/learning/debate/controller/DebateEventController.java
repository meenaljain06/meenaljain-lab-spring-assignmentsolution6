package com.great.learning.debate.controller;

import com.great.learning.debate.model.Student;
import com.great.learning.debate.service.DebateEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/students")
public class DebateEventController {
    @Autowired
    private DebateEventService debateEventService;

    @RequestMapping("/list")
    public String listStudents(Model model) {

        List<Student> students = debateEventService.findAll();

        model.addAttribute("Students", students);

        return "list-students";
    }

    @RequestMapping("/showFormForAdd")
    public String showFormForAdd(Model model) {

        Student student = new Student();

        model.addAttribute("Student", student);
        model.addAttribute("Heading", "Add a new Student");

        return "student-form";
    }

    @RequestMapping("/showFormForUpdate")
    public String showFormForUpdate(@RequestParam("studentId") int id, Model model) {

        Student student = debateEventService.findById(id);

        model.addAttribute("Student", student);
        model.addAttribute("Heading", "Update the Student Details");

        return "student-form";
    }

    @PostMapping("/save")
    public String saveStudent(@RequestParam("id") int id, @RequestParam("name") String name,
                              @RequestParam("department") String department, @RequestParam("country") String country) {

        Student student;

        if (id != 0) {
            student = debateEventService.findById(id);
            student.setName(name);
            student.setDepartment(department);
            student.setCountry(country);

        } else {
            student = new Student(name, department, country);
        }

        debateEventService.save(student);

        return "redirect:/students/list";
    }

    @RequestMapping("/delete")
    public String deleteStudent(@RequestParam("studentId") int id) {

        debateEventService.deleteById(id);

        return "redirect:/students/list";
    }

    @RequestMapping("/403")
    public ModelAndView accessDenied(Principal user) {
        ModelAndView model = new ModelAndView();

        if (user != null) {
            model.addObject("msg", "Hi " + user.getName()
                    + ", you do not have the permission to access this page!!!");
        } else {
            model.addObject("msg", "You do not have the permission to access this page!!!");
        }

        model.setViewName("403");

        return model;
    }
}
