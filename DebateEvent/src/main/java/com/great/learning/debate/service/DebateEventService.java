package com.great.learning.debate.service;

import com.great.learning.debate.model.Student;

import java.util.List;

public interface DebateEventService {
    public List<Student> findAll();

    public Student findById(int id);

    public void save(Student student);

    public void deleteById(int id);
}
