package com.great.learning.debate.service.impl;

import com.great.learning.debate.model.Student;
import com.great.learning.debate.service.DebateEventService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DebateEventServiceImpl implements DebateEventService {
    @Override
    public List<Student> findAll() {
        return null;
    }

    @Override
    public Student findById(int id) {
        return null;
    }

    @Override
    public void save(Student student) {

    }

    @Override
    public void deleteById(int id) {

    }
}
